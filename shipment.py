# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pyson import Eval, Not
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.modules.stock_party_warehouse.model import PartyWarehouseMixin


class ShipmentInternal(PartyWarehouseMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'

    party = fields.Many2One('party.party', 'Party',
        domain=[('warehouses', '!=', None)],
        states={
            'readonly': (~Eval('state').in_(['request', 'draft'])
                    | Eval('moves', [0])
                    | (Eval('from_location') & Eval('to_location'))),
            'invisible': (
                Not(Eval('party'))
                & (Eval('state') != 'draft'))
        },
        depends=['state', 'moves', 'from_location', 'to_location'])

    @fields.depends('party', 'from_location', 'to_location',
        methods=['_get_party_warehouse_pattern'])
    def on_change_from_location(self):
        if (self.party
                and self.from_location
                and not self.to_location):
            warehouse = self.get_party_warehouse_used(
                **self._get_party_warehouse_pattern())
            if self.from_location.warehouse != warehouse:
                self.to_location = warehouse.storage_location

    @fields.depends('party', 'from_location', 'to_location',
        methods=['_get_party_warehouse_pattern'])
    def on_change_to_location(self):
        if (self.party
                and self.to_location
                and not self.from_location):
            warehouse = self.get_party_warehouse_used(
                **self._get_party_warehouse_pattern())
            if self.to_location.warehouse != warehouse:
                self.from_location = warehouse.storage_location

    @classmethod
    def wait(cls, shipments, moves=None):
        for shipment in shipments:
            if shipment.party:
                wh = shipment.get_party_warehouse_used(
                    **shipment._get_party_warehouse_pattern())
                if (wh != shipment.from_location.warehouse
                        and wh != shipment.to_location.warehouse):
                    raise UserError(gettext('stock_party_warehouse.'
                        'msg_shipment_internal_party_warehouse',
                        shipment=shipment.rec_name,
                        warehouse=wh.rec_name))
        super().wait(shipments, moves=moves)


class ShipmentInReturn(PartyWarehouseMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.in.return'

    @property
    def party_with_warehouse(self):
        return self.supplier


class ShipmentOut(PartyWarehouseMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    @property
    def party_with_warehouse(self):
        return self.customer


class ShipmentOutReturn(PartyWarehouseMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out.return'

    @property
    def party_with_warehouse(self):
        return self.customer
