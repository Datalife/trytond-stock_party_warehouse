datalife_stock_party_warehouse
==============================

The stock_party_warehouse module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_party_warehouse/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_party_warehouse)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
