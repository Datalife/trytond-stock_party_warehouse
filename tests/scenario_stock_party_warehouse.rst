==============================
Stock Party Warehouse Scenario
==============================

Imports::

    >>> import datetime
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()

Install stock_party_warehouse::

    >>> config = activate_modules('stock_party_warehouse')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> production_loc, = Location.find([('type', '=', 'production')])
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> internal_loc = Location(name='Internal', type='storage')
    >>> internal_loc.save()
    >>> view_loc = Location()
    >>> view_loc.name = 'View location'
    >>> view_loc.type = 'view'
    >>> view_loc.save()


Add party warehouse parent to stock configuration::

    >>> Conf = Model.get('stock.configuration')
    >>> conf = Conf(1)
    >>> conf.party_warehouse_parent = view_loc
    >>> conf.save()

Party Warehouse::

    >>> PartyWarehouse = Model.get('party.party.warehouse')
    >>> party_warehouse = PartyWarehouse.find([])
    >>> len(party_warehouse)
    0

Create parties::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party')
    >>> party.save()
    >>> party2 = Party(name='Party 2')
    >>> party2.save()
    >>> party3 = Party(name='Party 3')
    >>> party3.save()
    >>> party4 = Party(name='Party 4')
    >>> party4.save()
    >>> party5 = Party(name='Party 5')
    >>> party5.save()
    >>> bool(party5.create_warehouse)
    False

Add parties warehouses::

    >>> add_warehouse = Wizard('party.party.create_warehouse', [party, party2])
    >>> add_warehouse.form.warehouse_per_party = True
    >>> add_warehouse.form.parent == conf.party_warehouse_parent
    True
    >>> add_warehouse.execute('add_')
    >>> party.warehouse != None
    True
    >>> wh = party.warehouse
    >>> wh.name == party.name
    True
    >>> wh.input_location == wh.output_location == wh.storage_location
    True
    >>> wh.input_location.name == party.name
    True
    >>> wh.production_location == production_loc
    True
    >>> party2.warehouse != None
    True
    >>> wh2 = party2.warehouse
    >>> wh2.name == party2.name
    True
    >>> wh2.input_location == wh2.output_location == wh2.storage_location
    True
    >>> wh2.input_location.name == party2.name
    True
    >>> wh2.production_location == production_loc
    True

    >>> add_warehouse = Wizard('party.party.create_warehouse', [party3])
    >>> add_warehouse.form.warehouse = warehouse_loc
    >>> add_warehouse.form.warehouse_per_party = False
    >>> add_warehouse.execute('add_')
    >>> party3.warehouse == warehouse_loc
    True

    >>> add_warehouse = Wizard('party.party.create_warehouse', [party4, party5])
    >>> add_warehouse.form.warehouse_name = 'Warehouse for party 4 & 5'
    >>> add_warehouse.form.warehouse_per_party = False
    >>> add_warehouse.execute('add_')
    >>> party4.reload()
    >>> party5.reload()
    >>> party4.warehouse == party5.warehouse
    True
    >>> bool(party5.create_warehouse)
    True

Check create Party Warehouse::

    >>> PartyWarehouse = Model.get('party.party.warehouse')
    >>> party_warehouses = PartyWarehouse.find([])
    >>> len(party_warehouses)
    5
    >>> party_warehouse, = PartyWarehouse.find([('party', '=', party)])
    >>> party_warehouse.warehouse == party.warehouse
    True

Add party with create_warehoues check::

    >>> party6 = Party(name='Party 6')
    >>> party6.create_warehouse = True
    >>> party6.save()
    >>> bool(party6.warehouse)
    True
    >>> party6.warehouse.name == party6.name
    True
    >>> party6.warehouse.parent == view_loc
    True
    >>> party6.warehouse.storage_location == party6.warehouse.input_location == party6.warehouse.output_location
    True

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.save()
    >>> product, = template.products


Create Internal Shipment::

    >>> Shipment = Model.get('stock.shipment.internal')
    >>> StockMove = Model.get('stock.move')
    >>> shipment = Shipment()
    >>> shipment.party = party
    >>> shipment.planned_date = today
    >>> not shipment.to_location
    True
    >>> shipment.from_location = internal_loc
    >>> not shipment.to_location
    False
    >>> shipment.to_location == party.warehouse.storage_location
    True
    >>> shipment.to_location = storage_loc
    >>> move = shipment.moves.new()
    >>> move.product = product
    >>> move.quantity = 1
    >>> move.from_location = internal_loc
    >>> move.to_location = storage_loc
    >>> move.currency = company.currency
    >>> shipment.click('wait')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: "From Location" or "To Location" in Internal Shipment "1" must be a location on Party Warehouse "Party". - 
    >>> shipment.moves.remove(shipment.moves[0])
    >>> shipment.to_location = party.warehouse.storage_location
    >>> move = shipment.moves.new()
    >>> move.product = product
    >>> move.quantity = 1
    >>> move.from_location = internal_loc
    >>> move.to_location = party.warehouse.storage_location
    >>> move.currency = company.currency
    >>> shipment.click('wait')
    >>> shipment.state
    'waiting'